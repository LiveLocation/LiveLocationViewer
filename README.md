# LiveLocation Viewer
1. Get a tilelayer provider. The default here is mapbox, but this is not an endorsement but rather just an arbitrary choice. See the leaflet docs for more information.
2. Change the values in the index.html in the inline javascript.
3. Serve it.
